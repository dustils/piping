#!/bin/bash

date
echo "(re)starting"

if ! test -f "output-$1-t1";
then
    echo "generating first output file..."
    sleep 90
    date > output-$1-t1
    echo "done"
fi

if ! test -f "output-$1-t2";
then
    echo "generating second output file..."
    sleep 90
    date > output-$1-t2
    echo "done"
fi
