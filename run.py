#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Script to run job pipelines.

This script allows to run an ordered series of jobs in different
environments. After being defined in a configuration file, jobs can
be run locally or submitted to a workload manager such as Slurm. At
runtime, this script reads the configuration file associated with the
pipeline to generate and execute the appropriate shell scripts.

"""

from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime, timezone
from logging import getLogger
from os import makedirs, path
from pathlib import Path
from string import Template
from subprocess import CalledProcessError, check_output, PIPE, run
from sys import exit as sys_exit
from time import sleep


logger = getLogger(__name__)


class Pipeline:
    """This class represents an ordered series of jobs.

    Attributes
    ----------
    defaults : dict of str
        Default value for job configuration.
    name : str
        Pipeline name.
    date : str
        Pipeline creation date.
    config : ConfigParser
        Pipeline configuration.
    stages : dict of str
        Name of the jobs at each stage.
    order : list of str
        Execution order of the stages.

    """

    defaults = {
        'directory': 'scratch/$PIPING_DATE-$PIPING_PIPELINE',
        'script': '$PIPING_PIPELINE-$PIPING_STAGE-$PIPING_JOB.job',
        'logging': '$PIPING_PIPELINE-$PIPING_STAGE-$PIPING_JOB.log',
    }

    def __init__(self, config_path):
        """Instantiate a pipeline representation.

        Parameters
        ----------
        config_path : str
            Path to the pipeline configuration file.

        """
        logger.debug("defining pipeline variables")
        self.name = Path(config_path).stem
        self.date = datetime.now(timezone.utc).strftime("%Y%m%dT%H%M%SZ")

        logger.debug("loading pipeline configuration file %s", config_path)
        self.config = ConfigParser(defaults=self.defaults)
        with open(config_path, 'r', encoding='utf-8') as file:
            self.config.read_file(file)

        logger.debug("defining job stages and order")
        self.stages = {}
        for job in self.config.sections():
            stage = self.config.get(job, 'stage')
            self.stages.setdefault(stage, []).append(job)
        self.order = sorted(self.stages.keys())

    def variables(self, job):
        """Return the environment variables of the job.

        Parameters
        ----------
        job : str
            Name of the job in the pipeline.

        Returns
        -------
        dict of str
            Job environment variables.

        """
        variables = {
            'PIPING_PIPELINE': self.name,
            'PIPING_DATE': self.date,
            'PIPING_STAGE': self.config.get(job, 'stage'),
            'PIPING_JOB': job,
        }
        return variables

    def substitute(self, template, job):
        """Return the template with expanded environment variables.

        Parameters
        ----------
        template : str
            Template containing variable names.
        job : str
            Name of the job in the pipeline.

        Returns
        -------
        str
            Expanded template.

        """
        variables = self.variables(job)
        result = Template(template).safe_substitute(variables)
        result = path.expanduser(result)
        result = path.expandvars(result)
        return result


class SlurmPipeline(Pipeline):
    """This class allows to submit a pipeline to Slurm.

    """

    def script(self, job):
        """Return the Slurm job script content.

        Parameters
        ----------
        job : str
            Name of the job in the pipeline.

        Returns
        -------
        str
            Job script content.

        """
        logger.debug("retrieving %s job settings", job)
        section = self.config[job]
        logfile = self.substitute(section.get('logging', ''), job)
        variables = self.variables(job)
        content = "\n"
        requeue = "$SLURM_JOB_ID"

        logger.debug("mapping options")
        options = {
            'job-name': job,
            'nodes': section.get('nodes', '1'),
            'mail-user': section.get('email', ''),
            'mail-type': section.get('notification', ''),
            'time': section.get('time', '24:00:00'),
            'partition': section.get('partition', ''),
            'mem': section.get('mem', '0'),
            'error': logfile,
            'output': logfile,
            'open-mode': section.get('open-mode', 'append'),
            'exclusive': section.getboolean('exclusive', True),
        }

        logger.debug("export environment variables")
        content += "".join(f"export {k}={v}\n" for k, v in variables.items())

        logger.debug("define array")
        array = {k[5:]: v for k, v in section.items() if k.startswith('array')}
        lengths = [len(v.split()) for v in array.values()]
        if not all(length == lengths[0] for length in lengths[1:]):
            raise ValueError("arrays should be the same length")
        if array:
            options['error'] += '-%a'
            options['output'] += '-%a'
            options['array'] = f"0-{lengths[0]-1}"
            if section.get('tasks', ''):
                options['array'] += f"%{int(section['tasks'])}"
            requeue = "${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
            content += "\n"
        for key, value in array.items():
            content += f"arr{key}=({value})\n"
        for key in array:
            content += f"var{key}=${{arr{key}[$SLURM_ARRAY_TASK_ID]}}\n"

        logger.debug("load modules")
        if section.get('modules', ''):
            content += "\nmodule purge\n"
            for module in section.get('modules').split():
                content += f"module load {module}\n"

        logger.debug("activate virtual environment")
        if section.get('venv', ''):
            source = path.join(section.get('venv'), 'bin', 'activate')
            content += f"\nsource {source}\n"
        elif section.get('conda', ''):
            content += f"\nconda activate {section.get('conda')}\n"

        logger.debug("implement requeuing mechanism")
        content += (
            "\nterminate () {\n"
            "    echo caught SIGTERM\n"
            '    echo terminating "$pid"\n'
            '    kill -TERM "$pid"\n'
            '    wait "$pid"\n'
            f"    echo requeuing {requeue}\n"
            f"    scontrol requeue {requeue}\n"
            "}\n"
            "\ntrap terminate SIGTERM\n"
            f"\nsrun {section['command']} &\n"
            "pid=$!\n"
            'wait "$pid"\n'
        )

        logger.debug("pass options")
        header = "#!/bin/bash\n"
        for key, value in options.items():
            if isinstance(value, bool) and value:
                header += f"#SBATCH --{key}\n"
            elif isinstance(value, str) and value:
                header += f"#SBATCH --{key} {value}\n"

        return header + content

    def submit(self, job, dependencies):
        """Submit a job to Slurm and return its job id.

        Parameters
        ----------
        job : str
            Name of the job to be submitted.
        dependencies : list of str
            Id of the jobs to be completed before.

        Returns
        -------
        str
            Submitted job id.

        """
        logger.debug("retrieving %s job settings", job)
        section = self.config[job]
        workdir = self.substitute(section['directory'], job)
        jobfile = self.substitute(section['script'], job)

        logger.debug("initializing work directory")
        makedirs(workdir, exist_ok=True)
        with open(path.join(workdir, jobfile), 'w', encoding='utf-8') as file:
            file.write(self.script(job))

        logger.debug("submitting job")
        command = ['cd', workdir, ';', 'sbatch']
        if len(dependencies) != 0:
            command.append('--dependency')
            dependency = section.get('dependency', 'afterok')
            dependencies = [f'{dependency}:{jobid}' for jobid in dependencies]
            command.append(','.join(dependencies))
        command.append('--parsable')
        command.append(jobfile)
        command = ' '.join(command)
        try:
            result = check_output(command, text=True, shell=True, stderr=PIPE)
            jobid = result.strip()
            message = f"submitted {job} ({jobid})"
            if len(dependencies) != 0:
                message += f" {' '.join(dependencies)}"
            print(message)
            return jobid
        except CalledProcessError as exc:
            raise RuntimeError(exc.returncode, exc.stderr) from exc

    def run(self, *dependencies):
        """Submit the pipeline to Slurm.

        Parameters
        ----------
        *dependencies : list of str
            Id of the jobs to be completed before.

        """
        for rank in self.order:
            ids = []
            for job in self.stages[rank]:
                sleep(1)
                ids.append(self.submit(job, dependencies))
            dependencies = ids


class LocalPipeline(Pipeline):
    """This class allows to execute a pipeline locally.

    """

    def script(self, job):
        """Return the job shell script.

        Parameters
        ----------
        job : str
            Name of the job in the pipeline.

        Returns
        -------
        str
            Job script content.

        """
        logger.debug("retrieving %s job settings", job)
        section = self.config[job]
        variables = self.variables(job)
        content = "#!/bin/bash\n\n"

        logger.debug("export environment variables")
        content += "".join(f"export {k}={v}\n" for k, v in variables.items())

        logger.debug("define array")
        array = {k[5:]: v for k, v in section.items() if k.startswith('array')}
        lengths = [len(v.split()) for v in array.values()]
        if not all(length == lengths[0] for length in lengths[1:]):
            raise ValueError("arrays should be the same length")
        if array:
            content += "\n"
        for key, value in array.items():
            content += f"arr{key}=({value})\n"

        logger.debug("activate virtual environment")
        if section.get('venv', ''):
            source = path.join(section.get('venv'), 'bin', 'activate')
            content += f"\nsource {source}\n"
        elif section.get('conda', ''):
            content += f"\nconda activate {section.get('conda')}\n"

        logger.debug("execute command")
        if array:
            content += (
                f"\nfor i in {{0..{lengths[0]-1}}}\n"
                f"do\n"
            )
            for key in array:
                content += f"    var{key}=${{arr{key}[$i]}}\n"
            content += (
                f"    {section.get('command')}\n"
                f"done"
            )
        else:
            content += "\n" + section.get('command')

        return content

    def submit(self, job):
        """Execute a job locally.

        Parameters
        ----------
        job : str
            Name of the job to be executed.

        """
        logger.debug("retrieving %s job settings", job)
        section = self.config[job]
        workdir = self.substitute(section['directory'], job)
        jobfile = self.substitute(section['script'], job)
        logfile = self.substitute(section['logging'], job)

        logger.debug("initializing work directory")
        makedirs(workdir, exist_ok=True)
        with open(path.join(workdir, jobfile), 'w', encoding='utf-8') as file:
            file.write(self.script(job))

        logger.debug("executing job")
        print(f"running {job}")
        command = f"cd {workdir}; bash {jobfile} > {logfile} 2>&1"
        result = run(command, shell=True, check=False)
        if result.returncode:
            print(f"exited with code {result.returncode}, see {logfile}")
            sys_exit(result.returncode)

    def run(self, *args):
        """Run the pipeline.

        """
        _ = args
        for rank in self.order:
            for job in self.stages[rank]:
                self.submit(job)


if __name__ == '__main__':

    launchers = {
        'local': LocalPipeline,
        'slurm': SlurmPipeline,
    }

    parser = ArgumentParser(
        prog='./run.py',
        description="pipeline launcher",
    )
    parser.add_argument(
        'pipeline',
        help="path to the pipeline configuration file",
        metavar='path',
    )
    parser.add_argument(
        '-d', '--dependency',
        help="id of the jobs to be completed before",
        nargs='+',
        metavar='id',
        default=[],
    )
    group = parser.add_mutually_exclusive_group()
    for name, lancher in launchers.items():
        group.add_argument(
            f'--{name}',
            action='store_const',
            const=name,
            dest='launcher',
            help=f"launch with {lancher.__name__}",
        )
    arguments = parser.parse_args()

    try:
        launcher = launchers.get(arguments.launcher, SlurmPipeline)
        pipeline = launcher(arguments.pipeline)
        pipeline.run(*arguments.dependency)
    except KeyboardInterrupt:
        print("program interrupted by Control-C")
        sys_exit(130)
