# The lines below do not correspond to a job but configure the default
# settings. The name "DEFAULT" is reserved for this use.
[DEFAULT]
partition = all.q
directory = scratch/$PIPING_DATE-$PIPING_PIPELINE-test
script = $PIPING_PIPELINE-$PIPING_STAGE-$PIPING_JOB.sh
logging = $PIPING_PIPELINE-$PIPING_STAGE-$PIPING_JOB.txt
email = your@email.com
notification = FAIL
time = 00:02:00

# The "copy" job defined below is an example of how to copy data to the
# working directory created when the pipeline was launched.
[copy]
stage = 0
command = cp ../../material/executable.sh . --verbose

# The "build" job defined below will use the modules package to load
# two modules. After expansion of the variables, the command executed
# will be: "./executable.sh build"
[build]
stage = 1
modules = gcc/9.3.0 mpi/openmpi-4.1.1-gcc9-3
command = ./executable.sh $PIPING_JOB

# The "array-1" job defined below is an example of how to run a command
# for different values of a variable using Slurm arrays. During its
# execution, this job will generate two tasks with the commands:
# "./executable.sh array-1-a" and "./executable.sh array-1-b".
[array-1]
stage = 2
array = a b
command = ./executable.sh $PIPING_JOB-$var

# The "array-2" job defined below follows the example of the previous
# job but adding a variable. During its execution, this job will
# generate two tasks with the commands:
# "./executable.sh array-2-a-c" and "./executable.sh array-2-b-d".
[array-2]
stage = 2
array_0 = a b
array_1 = c d
command = ./executable.sh $PIPING_JOB-${var_0}-${var_1}

# The "post-process" job defined bellow shows how to run a job on
# multiple nodes.
[post-process]
stage = 3
nodes = 2
command = ./executable.sh $PIPING_JOB
# In Slurm versions prior to 19.05.3, arrays with requeued tasks always
# return an exit code different from 0. The following line allows to
# bypass this bug. Remove it if your version of Slurm is >= 19.05.3.
dependency = afterany

# This last job allows to store the produced data in an archive. Here,
# the type of event notified by email is modified in order to inform
# the user when the pipeline is completed.
[archive]
notification = END,FAIL
stage = 5
command = tar -cvf $PIPING_DATE-$PIPING_PIPELINE.tar output-*
