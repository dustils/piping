# Piping

> What does it mean?

The name Piping has been chosen to suggest a set of job pipelines.

> What is it for?

Piping makes it easy to set up and run job pipelines in different environments.

> Is it hard to use?

Just edit a configuration file and run the program.

## Background

When working with a workload manager like Slurm, one can spend a lot of time writing and submitting job scripts.
Suppose you want to submit a series of jobs with dependencies and various specifications.
Creating all the job scripts then retrieving and providing the dependency job ids for each submission can quickly become tedious.
In this case, a certain degree of automation would then be appreciated.

When preparing or modifying a pipeline, it can also be very useful to test the execution on your computer. This would allow you to check the progress of the jobs and quickly identify bugs without having to connect to the cluster or wait for resources. Therefore, it could be very advantageous to have a single file containing the essence of the pipeline and its jobs and that could be converted into scripts for Slurm or for your computer depending on the need.

Finally, it is sometimes quite difficult to estimate the duration of a job.
Besides, some clusters limit the maximum duration to a few hours or days.
An automatic requeuing mechanism is often the solution in this case, but can be annoying to implement.
In short, a simple way to configure and submit pipelines with an auto-requeuing feature could save valuable design and calculation time.

## Features

Piping proposes a script that allows to run ordered series of jobs.
The preparation of a pipeline is done in a configuration file.
At runtime, this script reads the configuration file associated with the pipeline and translates its contents into directives for Slurm or for your computer.
When using the launcher for Slurm, the generated job scripts implement an automatic requeuing mechanism.
So if one of your jobs is not finished by the end of the time limit, a SIGTERM signal is sent a few seconds before the job is finished and placed back in the queue.

## Installation

You can simply clone or download the project.
But since the essence of the project is contained in the [run.py](/run.py) script, you can also simply copy and paste its content.

## Usage

Check that the [run.py](/run.py) script is executable so that you can run it without typing "python3" in the command line.
Then, you can run the vanilla example pipeline configured in the [pipelines/vanilla.conf](/pipelines/vanilla.conf) file with the command:

```bash
./run.py pipelines/vanilla.conf --local
```

The `--local` option allows you to run the pipeline on your computer.
To submit to Slurm, remove this option or use `--slurm` instead.
You can store your pipeline configuration files anywhere as long as you pass the correct path as an argument when calling the script.

All that remains is to configure your own pipeline.
To do this, create a file in the same format as [pipelines/customized.conf](/pipelines/customized.conf).
Except for the `DEFAULT` section (which defines the default values for all jobs), each section of the file corresponds to the configuration of a job of the pipeline.
For each job you can configure the following fields:

* `stage` **(required)**: String defining at which stage the job must be executed. The execution follows the lexicographic order and jobs that have the same stage value can be executed in parallel. By default, all jobs in a stage must be completed without error before moving to the next stage.
* `command` **(required)**: Main shell command of the job. This entry contains the essence of what the job does. As the SIGTERM signal is sent only once, it is strongly recommended to write only one command per job (i.e. not to put several commands separated by semicolons in this field).
* `directory` **(optional)**: Path to the directory in which the job is executed. The directory is created if it does not exist when the pipeline is launched. Other paths specified in the configuration file will be resolved from this directory if they are relative. By default, the name of the directory contains the creation date of the pipeline and the name of the pipeline so that each launch of a pipeline corresponds to a different directory.
* `modules` **(optional)**: The list of modules to load with the `module load <name>` command. Using this field assumes that the [modules package](https://github.com/cea-hpc/modules) is installed, but this is usually the case on most clusters. To indicate multiple modules, separate module names with spaces.
* `venv` **(optional)**: The path to the Python virtual environment to activate. Using this field will place the `source <path>/bin/activate` command in the first lines of the job script. This command must be used for virtual environments created with the [venv](https://docs.python.org/3/library/venv.html) package.
* `conda` **(optional)**: Name to the Python virtual environment to activate. Using this field will place the `conda activate <name>` command in the first lines of the job script. This command must be used for virtual environments created with Conda.
* `array<suffix>` **(optional)**: Allows to execute the job for different values of a variable named `$var<suffix>`. Specify the values taken by `$var<suffix>` separated by spaces and use `$var<suffix>` or `${var<suffix>}` in the command line of the job. The suffix can be replaced by a string of alphanumeric characters and underscore. This allows to define several arrays with their associated variables. To limit the number of simultaneous tasks, add the field `tasks` followed by an integer.
* `dependency` **(optional)**: Choose among `after`, `afterany`, `afterburstbuffer`, `aftercorr`, `afternotok`, `afterok` to select the condition to be respected by dependencies to start the job. This corresponds to what is passed to Slurm option [--dependency](https://slurm.schedmd.com/sbatch.html#OPT_dependency) before the list of job ids.
* `script` **(optional)**: Path to the job script file. If the path is relative, it will be evaluated from the path specified in the `directory` field.
* `logging` **(optional)**: Path to the job log file. If the path is relative, it will be evaluated from the path specified in the `directory` field.
* `partition` **(optional)**: Value passed to Slurm option [--partition](https://slurm.schedmd.com/sbatch.html#OPT_partition).
* `nodes` **(optional)**: Value passed to Slurm option [--nodes](https://slurm.schedmd.com/sbatch.html#OPT_nodes).
* `email` **(optional)**: Value passed to Slurm option [--mail-user](https://slurm.schedmd.com/sbatch.html#OPT_mail-user).
* `notification` **(optional)**: Value passed to Slurm option [--mail-type](https://slurm.schedmd.com/sbatch.html#OPT_mail-type).
* `time` **(optional)**: Value passed to Slurm option [--time](https://slurm.schedmd.com/sbatch.html#OPT_time).

To define the fields (such as `directory`, `script` or `logging`) you can use the following environment variables:

* `$PIPING_PIPELINE`: Name of the pipeline (corresponds to the stem of the pipeline configuration file name).
* `$PIPING_DATE`: UTC date of submission of the pipeline in ISO `YYYYMMDDTHHMMSSZ` format.
* `$PIPING_STAGE`: Stage of the job.
* `$PIPING_JOB`: Name of the job (corresponds to the name of the section in the configuration file).

## Credits

* Dunstan Becht

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
